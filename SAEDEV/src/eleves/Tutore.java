package eleves;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * 
 *@author loic.lecointe.etu 
 *
 *It's the class that will allow to create the different Tutors extends of the class Eleve
 *
 */
public class Tutore extends Eleve{
	
	/**
	 * It's the main constructor of the class
	 * @param nom
	 * @param prenom
	 * @param annee
	 */
	public Tutore(String nom, String prenom) {
		super(nom, prenom,1);
	}
	
	/**
	 * It is the second constructor of the class made by overloading.
	 * @param nom
	 * @param prenom
	 * @param matiere
	 */
	public Tutore(String nom, String prenom, HashMap<Matiere,Double> grades) {
		super(nom, prenom, 1);
		this.notes.putAll(grades);
	}
	/**
	 * Void constructor for Json deserialization
	 */
	public Tutore() {
		super(null);
	}
	
	/**
	 * @Return the different subjects of the Matiere enum 
	 */
	public ArrayList<Matiere> getMatiere() {
		ArrayList<Matiere> res = new ArrayList<>();
		res.addAll(this.notes.keySet());
		return res;
	}
	
	
	/**
	 * ToString method
	 * @return String representation
	 */
	public String toString() {
		return "Tutore " + super.toString();
	}
	
}
