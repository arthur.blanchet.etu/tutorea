package eleves;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.core.exc.StreamWriteException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.ObjectMapper;

import graphes.Affectation;
import graphes.AffectationManager;


/**
 * Class with static methods to save and load with Jackson library
 * 
 * @author arthur.blanchet.etu
 *
 */
public class SaveLoad {
	/**
	 * Static method to save a affectation instance in jsons
	 * @param a
	 */
	public static void saveAffectation(AffectationManager a) {
		String path = System.getProperty("user.dir")+"/donnees/";
		ObjectMapper om = new ObjectMapper();
		try {
			ArrayList<Tuteur> newTuteur = new ArrayList<>();
			for(Tuteur t : a.getTuteurs()) {
				if(!t.getNom().equals("Null")) {
					newTuteur.add(t);
				}
			}
			ArrayList<Tutore> newTutore = new ArrayList<>();
			for(Tutore t : a.getTutores()) {
				if(!t.getNom().equals("Null")) {
					newTutore.add(t);
				}
			}
			om.writeValue(new File(path+"/json/tuteur.json"), newTuteur);
			om.writeValue(new File(path+"/json/tutores.json"), newTutore);
			om.writeValue(new File(path+"/json/nonaffecte.json"), a.getNonAffectation());
			om.writeValue(new File(path+"/json/affectation.json"), a.getAffectations());

		} catch (StreamWriteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DatabindException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * Static method to load json to a affectation instance
	 * @param a affectation instance
	 */
	public static void loadAffectation(AffectationManager a) {
		String path = System.getProperty("user.dir")+"/donnees/json/";
		ObjectMapper om = new ObjectMapper();
		try {
			a.getTuteurs().addAll(om.readValue(new File(path+"tuteur.json"),new TypeReference<List<Tuteur>>() {}));
			a.getTutores().addAll(om.readValue(new File(path+"tutores.json"),new TypeReference<List<Tutore>>() {}));
			a.getNonAffectation().addAll(om.readValue(new File(path+"nonaffecte.json"),new TypeReference<List<Eleve>>() {}));
			
			a.getAffectations().addAll(om.readValue(new File(path+"affectation.json"), new TypeReference<List<Affectation>>() {}));
			
			for(Affectation aff : a.getAffectations()) {
				a.getTuteurs().get(a.getTuteurs().indexOf(aff.getTuteur())).addAffectation(aff.getTutore());
				a.getTutores().get(a.getTutores().indexOf(aff.getTutore())).addAffectation(aff.getTuteur());

			}
			
		} catch (StreamReadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DatabindException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
