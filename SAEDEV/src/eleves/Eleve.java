package eleves;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Representation class for all students
 * 
 * @author ehis.okpebho.etu
 *
 */
@JsonIgnoreProperties(value = { "affectation"})
public abstract class Eleve {
	/**
	 * Student last name
	 */
	
	private String nom;
	
	/**
	 * Student first name
	 */
	
	private String prenom;
	
	
	
	private int annee;
	
	
	/**
	 * List for student assignments
	 */
	protected ArrayList<Eleve> affectation;
	
	/**
	 * List for student subjects and averages
	 */
	
	protected Map<Matiere,Double> notes;
		
	/**
	 * Class constructor
	 * 
	 * @param nom Student last name
	 * @param prenom Student first name
	 * @param note Student grade average
	 * @param annee Student year
	 */
	
	public Eleve(String nom, String prenom, int annee) {
		this.nom = nom;
		this.prenom = prenom;
		this.annee = annee;
		this.affectation = new ArrayList<Eleve>();
		this.notes = new HashMap<Matiere,Double>();
	}
	

	
	/**
	 * Name only constructor
	 * 
	 * @param nom
	 */
	
	public Eleve(String nom) {
		this(nom,"",0);
	}
	
	/**
	 * Gets student last name
	 * 
	 * @return student last name
	 */
	
	public String getNom() {
		return nom;
	}

	/**
	 * Sets student last name
	 * 
	 * @param nom Last name to set
	 */
	
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * Gets student first name
	 * 
	 * @return student first name
	 */
	
	public String getPrenom() {
		return prenom;
	}

	/**
	 * Sets student first name
	 * 
	 * @param prenom First name to set
	 */
	
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	/**
	 * Gets student grades average
	 * 
	 * @return student grades average
	 */
	
	public double getNote(Matiere mat) {
		return this.notes.get(mat);
	}

	/**
	 * Sets student grades average
	 * 
	 * @param note Grades average to set
	 */
	
	public void setNote(Matiere mat,double note) {
		this.notes.replace(mat,note);
	}
	
	/**
	 * Adds a grade with a subject
	 */
	public void addNote(Matiere mat, double note) {
		this.notes.put(mat, note);
	}
	
	/**
	 * Adds all grade with subject
	 */
	public void addAllNote(HashMap<Matiere,Double> map) {
		this.notes.putAll(map);
	}
	
	/**
	 * Gets all subjects with their grades
	 * @return
	 */
	public Map<Matiere,Double> getNotes(){
		return this.notes;
	}
	
	/**
	 * Gets the student year
	 * 
	 * @return student year
	 */
	
	public int getAnnee() {
		return this.annee;
	}

	/**
	 * Sets student year
	 * 
	 * @param annee Year to set
	 */
	
	public void setAnnee(int annee) {
		this.annee = annee;
	}
	
	/**
	 * Add an assignment to the current student
	 * 
	 * @param e Student to assign
	 */
	
	public void addAffectation(Eleve e) {
		this.affectation.add(e);
	}
	/**
	 * Add all assignments from a ArrayList
	 * @param e
	 */
	public void addAllAffectation(ArrayList<Eleve> e) {
		this.affectation.addAll(e);
	}
	
	/**
	 * Getter of assignments
	 * @return
	 */
	public ArrayList<Eleve> getAffectation(){
		return this.affectation;
	}
	
	/**
	 * Returns a string representation for the current student
	 * 
	 * @return student string representation
	 */
	@Override
	public String toString() {
		
		return nom.toUpperCase() + " " + prenom + " Annee " + annee + " - "+ notes;
	}


	/**
	 * Returns the hashcode of the name and surname
	 * 
	 * @return hashcode representation of the object
	 */
	@Override
	public int hashCode() {
		return Objects.hash(nom, prenom);
	}


	/**
	 * Equals methods based on name and surname
	 * 
	 * @return boolean equals
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Eleve other = (Eleve) obj;
		return Objects.equals(nom, other.nom) && Objects.equals(prenom, other.prenom);
	}



	
	
	
}
