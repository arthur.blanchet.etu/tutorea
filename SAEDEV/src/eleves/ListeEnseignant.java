package eleves;

import java.util.HashMap;

/**
 * Teacher list with logins and subjects
 * @author arthur blanchet
 *
 */
public class ListeEnseignant{
	/**
	 * Map of login and password
	 */
	public HashMap<String,String> enseignant = new HashMap<>();
	/**
	 * Map of login and subjects
	 */
	public HashMap<String,Matiere> matiere = new HashMap<>();
	
	/**
	 * Base constructor
	 */
	public ListeEnseignant(){
		enseignant.put("dev", "dev");
		enseignant.put("ihm", "ihm");
		enseignant.put("general", "general");
		enseignant.put("graphes", "graphes");
		matiere.put("dev", Matiere.DEV);
		matiere.put("ihm", Matiere.IHM);
		matiere.put("graphes", Matiere.GRAPHES);
		matiere.put("general", Matiere.GENERAL);

	}
	
}