package eleves;

import java.util.ArrayList;
import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Representation class for tutors which extends {@link Eleve} class
 * 
 * @author ehis.okpebho.etu
 *
 */
public class Tuteur extends Eleve {
	
	/**
	 * Class constructor
	 * 
	 * @param nom Tutor last name
	 * @param prenom Tutor first name
	 * @param note Tutor grades average
	 * @param annee Tutor year
	 */
	public Tuteur(String nom, String prenom, int annee) {
		super(nom, prenom, annee);
	}
	
	/**
	 * Class constructor with subjects
	 * 
	 * @param nom Tutor last name
	 * @param prenom Tutor first name
	 * @param note Tutor grades average
	 * @param annee Tutor year
	 * @param matiere Tutor subject
	 */
	public Tuteur(String nom, String prenom, int annee, HashMap<Matiere,Double> matiere) {
		super(nom, prenom, annee);
		this.notes.putAll(matiere);
	}
	
	/**
	 * Void constructor for Json deserialization
	 */
	public Tuteur() {
		super(null);
	}
	
	/**
	 * Gets tutor subject
	 * 
	 * @return tutor subject
	 */
	@JsonIgnore
	public Matiere getMatiere() {
		return (Matiere) this.notes.keySet().toArray()[0];
	}
	
	/**
	 * Method to add all affectation from a list of tutees
	 * @param value list of tutees
	 */
	public void addAllAffectationFromTutore(ArrayList<Tutore> value) {
		this.affectation.addAll(value);
	}
	/**
	 * ToString method
	 * @return String representation of the instance
	 */
	@Override
	public String toString() {
		return "Tuteur " + super.toString();
	}
}
