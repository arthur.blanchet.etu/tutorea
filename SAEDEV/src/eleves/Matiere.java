
package eleves;

/**
 * 
 * @author loic.lecointe.etu
 * 
 * It is the different subjects, which can be chosen by the Tutors or Tutees
 * 
 */
public enum Matiere {
	GENERAL(200),DEV(30), GRAPHES(20), IHM(15);
	
	/**
	 * max size of the subjects for affectations
	 */
	private int taille;
	
	/**
	 * Base constructor
	 * @param taille
	 */
	Matiere(final int taille){
		this.taille = taille;
	}
	/**
	 * Getter of size
	 * @return
	 */
	public int getTaille() {
		return taille;
	}
}
