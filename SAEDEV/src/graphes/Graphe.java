package graphes;

import eleves.*;
import fr.ulille.but.sae2_02.graphes.*;

/**
 * 
 * @author loic.lecointe.etu
 * @author arthur.blanchet.etu
 * @author ehis.okpebho.etu
 * 
 *	It is class that allows the creation of graphs. 
 */
public class Graphe {
	
	/**
	 * 
	 * Static method to create a graph from a list of Tutors, Tutees and from different subjects.
	 * @param tuteurs
	 * @param tutores
	 * @param matiere
	 * @return a graph of Tutors, Tutees and different subjects.
	 * 
	 */
	public static void creationGraphe(AffectationManager aff, GrapheNonOrienteValue<Eleve> graphe,Matiere matiere) {

		
				
		for(Eleve t : aff.tuteursFiltres) {
			graphe.ajouterSommet((Eleve)t);
		}
		
		for(Eleve tutore : aff.tutoresFiltres) {
			graphe.ajouterSommet((Eleve)tutore);
		}
		
		
		for(Eleve t : aff.tutoresFiltres) {
			for(Eleve tuteur : aff.tuteursFiltres) {
			
				if(!graphe.contientArete(t, tuteur)) {
					if(t.getNote(matiere)!= 999 || tuteur.getNote(matiere) != 999) {
						graphe.ajouterArete(t, tuteur, aff.calculAff(tuteur, t,matiere));
	
					} else {
						graphe.ajouterArete(t, tuteur, 999);
					}
				}
			}
		}		
	}
}