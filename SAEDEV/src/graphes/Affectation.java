package graphes;

import eleves.Tuteur;
import eleves.Tutore;

/**
 * This class a an affectation between a tutor and tutee
 * @author arthur.blanchet.etu
 *
 */
public class Affectation {
	/**
	 * Tutor attribute
	 */
	protected Tuteur tuteur;
	/**
	 * Tutee attribute
	 */
	protected Tutore tutore;
	
	/**
	 * Base class constructor
	 * @param t
	 * @param tu
	 */
	public Affectation(Tuteur t, Tutore tu) {
		this.tuteur = t;
		this.tutore = tu;
	}
	/**
	 * Null constructor for deserialization
	 */
	public Affectation() {}
	
	/**
	 * Getter of tutor
	 * @return
	 */
	public Tuteur getTuteur() {
		return tuteur;
	}
	
	/**
	 * Getter of tutee
	 * @return
	 */
	public Tutore getTutore() {
		return tutore;
	}
}
