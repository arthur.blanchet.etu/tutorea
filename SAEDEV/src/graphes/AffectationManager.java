package graphes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;

import eleves.*;
import fr.ulille.but.sae2_02.graphes.Arete;
import fr.ulille.but.sae2_02.graphes.CalculAffectation;
import fr.ulille.but.sae2_02.graphes.GrapheNonOrienteValue;


public class AffectationManager {
	/**
	 * Liste des élèves tuteurs
	 */
	protected ArrayList<Tuteur> tuteurs;
	/**
	 * Liste des élèves tutorés
	 */
	protected ArrayList<Tutore> tutores;
	
	/**
	 * Liste des tuteurs filtrés
	 */
	protected ArrayList<Tuteur> tuteursFiltres;
	
	/**
	 * Liste des turorés filtrés
	 */
	protected ArrayList<Tutore> tutoresFiltres;
	
	/**
	 * Liste des élèves qui ne se feront pas affecter
	 */
	protected ArrayList<Eleve> nonAffectation;
	
	protected ArrayList<Affectation> listeAffectation;
	
	/**
	 * Constructeur de la classe
	 * @param tuteurs
	 * @param tutorés
	 * @param siMatiere
	 */
	public AffectationManager(ArrayList<Tuteur> tuteurs,ArrayList<Tutore> tutores) {
		this.tuteurs = tuteurs;
		this.tutores = tutores;
		this.nonAffectation = new ArrayList<>();
		this.listeAffectation = new ArrayList<>();
	}
	
	/**
	 * Constructeur d'initialisation 
	 */
	public AffectationManager() {
		this(new ArrayList<>(), new ArrayList<>());
	}
	
	public ArrayList<Affectation> getAffectations(){
		return this.listeAffectation;
	}
	
	public Affectation getOneAffectation(Tuteur t, Tutore tu){
		for(Affectation a : this.listeAffectation) {
			if(a.tuteur == t && a.tutore == tu) {
				return a;
			}
		}
		return null;
	}
	
	/**
	 * @return the tuteurs
	 */
	public ArrayList<Tuteur> getTuteurs() {
		return tuteurs;
	}

	/**
	 * @return the tutores
	 */
	public ArrayList<Tutore> getTutores() {
		return tutores;
	}

	/**
	 * @return the filtered tutors
	 */
	public ArrayList<Tuteur> getTuteursFiltres() {
		return tuteursFiltres;
	}

	/**
	 * @return the filtered tutees
	 */
	public ArrayList<Tutore> getTutoresFiltres() {
		return tutoresFiltres;
	}

	/**
	 * @return the nonAffectation
	 */
	public ArrayList<Eleve> getNonAffectation() {
		return nonAffectation;
	}

	/**
	 * Méthode pour filtrer les tuteurs
	 * @param filtreMoyenne Moyenne filtre 
	 */
	public void filtreTuteur(double filtreMoyenne,Matiere mat){
		for(Tuteur t : tuteurs) {
			if(t.getNote(mat)>=filtreMoyenne && tuteursFiltres.size()<mat.getTaille()) {
				tuteursFiltres.add(t);
			}
		}
	}
	
	/**
	 * Méthode pour filtrer les tutorés
	 * @param filtreMoyenne Moyenne filtre
	 */
	public void filtreTutore(double filtreMoyenne,Matiere mat){
		for(Tutore t : tutores) {
			if(t.getNote(mat)<=filtreMoyenne && tutoresFiltres.size()<mat.getTaille()) {
				tutoresFiltres.add(t);
			}
		}
	}
	
	/**
	 * Prends en paramètre un tuteur et un tutoré pour donner sa valeur d'affectation
	 * @param tuteur
	 * @param tutoré
	 * @return double affectation value between the tutor and tutee
	 */
	
	public double calculAff(Eleve tuteur, Eleve tutore, Matiere mat) {
		double res= 30;
		double avgTuteur=0;
		int size = 0;
		for(Eleve t : tuteurs) {
			if(t.getNote(mat)!=999) {
				avgTuteur+=t.getNote(mat);
				size++;
			}
		}
		avgTuteur=avgTuteur/size;
		
		double avgTutore=0;
		size = 0;
		for(Eleve t : tutores) {
			if(t.getNote(mat)!=999) {
				avgTutore+=t.getNote(mat);
				size++;
			}
		}
		avgTutore=avgTutore/size;
		
		double newTuteurNote = tuteur.getNote(mat);
		if(tuteur.getAnnee()==3) {
			newTuteurNote+=10;
		}
		if(tuteur.getNote(mat)>avgTuteur+2) {
			res--;
		}
		if(tutore.getNote(mat)<avgTutore-4) {
			res=res-10;
		} else if (tutore.getNote(mat)>avgTutore ){
			res = res + 10;
		}
		res+=tuteur.getAffectation().size()*10;
		
		double diffNoteTT = newTuteurNote-(20-tutore.getNote(mat));

		return diffNoteTT<res?res-diffNoteTT:999;
	}
	
	/**
	 * Method for filtering the tutors and tutees with a subject
	 * @param mat
	 */
	public void filtreMatiere(Matiere mat) {
		ArrayList<Tuteur> newTuteurs = new ArrayList<>();
		ArrayList<Tutore> newTutores = new ArrayList<>();
		for(Tuteur tuteur : tuteurs) {
			if(tuteur.getMatiere() == mat) {
				newTuteurs.add(tuteur);
			}
		}
		for(Tutore tutore : tutores) {
			if(tutore.getMatiere().contains(mat)) {
				newTutores.add(tutore);
			}
		}
		this.tuteursFiltres = newTuteurs;
		this.tutoresFiltres = newTutores;
	}
	
	
	/**
	 * Method to create the affectations from a graph and a subject
	 * @param graphe
	 * @param mat
	 */
	public void creationAffectation(Matiere mat) {
		this.tuteursFiltres = tuteurs;
		this.tutoresFiltres = tutores;
		harmoniser(Matiere.GENERAL);
		GrapheNonOrienteValue<Eleve> graphe = new GrapheNonOrienteValue<>();
		Graphe.creationGraphe(this, graphe, mat);
		
		List<Eleve> newTutore = new ArrayList<>(tutoresFiltres);
		List<Eleve> newTuteurs = new ArrayList<>(tuteursFiltres);
		List<Double> listAffec = new ArrayList<>();
		
		CalculAffectation<Eleve> ca = new CalculAffectation<Eleve>(graphe,newTuteurs,newTutore);
		List<Arete<Eleve>> aretes = ca.getAffectation();
		for(Arete<Eleve> a : aretes) {
			Tutore t = tutores.get(tutores.indexOf((Tutore) a.getExtremite1()));
			Tuteur tu = tuteurs.get(tuteurs.indexOf((Tuteur) a.getExtremite2()));
			if(tu.getNom().equals("Null")) {
				for(int i = 0; i < tutoresFiltres.size();++i) {
					listAffec.add(calculAff(tu,t,mat));
				}
				int index = listAffec.indexOf(Collections.min(listAffec));
				if(!t.getAffectation().contains(tuteurs.get(index))) {
					t.addAffectation(tuteurs.get(index));
					tuteurs.get(index).addAffectation(t);
					listeAffectation.add(new Affectation(tu,t));
				}
				
			} else if (!tu.getNom().equals("Null")&&!t.getNom().equals("Null")) {
				if(!t.getAffectation().contains(tu)) {
					t.addAffectation(tu);
					tu.addAffectation(t);
					listeAffectation.add(new Affectation(tu,t));
				}
				
			}
		}
		
	}
	
	
	/**
	 * Method for filling the list of tutors and tutees with null student
	 * @param matiere
	 */
	public void harmoniser(Matiere matiere) {
		
		int diffTT = tuteurs.size() - tutores.size();
		HashMap<Matiere,Double> siNonEgal = new HashMap<>();
		siNonEgal.put(matiere, 999.0);
		if(diffTT>0) {
			
			for(int i = 0; i < diffTT; i++) {
				Tutore vide = new Tutore("Null","Null",siNonEgal);
				tutoresFiltres.add(vide);
				
			}
		} else if(diffTT<0) {
			for(int i = 0; i>diffTT;i--) {
				Tuteur vide = new Tuteur("Null","Null",2,siNonEgal);
				tuteursFiltres.add(vide);	
				
			}
		}
	}
	
	
	
	/**
	 * Surchage method with a tutor
	 * @param t
	 */
	public void supprAffectation(Tuteur t) {
		for(Tuteur tuteur : this.tuteurs) {
			if(t.equals(tuteur)) {
				this.tuteurs.remove(tuteur);
				tuteur.getAffectation().clear();
				this.nonAffectation.add(tuteur);
				break;
			}
		}
	
		this.tuteurs = new ArrayList<>(new HashSet<>(tuteurs));
	}
	/**
	 * Surcharge method with a tutee
	 * @param t
	 */
	public void supprAffectation(Tutore t) {
		for(Tutore tutore : this.tutores) {
			if(t.equals(tutore)) {
				this.tutores.remove(tutore);
				tutore.getAffectation().clear();
				this.nonAffectation.add(tutore);
				break;
			}
		}
		this.tutores = new ArrayList<>(new HashSet<>(tutores));
	}
	
	public void supprAffectation(Eleve e) {
		this.nonAffectation.add(e);
	}
	
	public void supprAffectation(Affectation aff) {
		for(Affectation a : this.listeAffectation) {
			if(a.getTuteur() == aff.getTuteur() && a.getTutore() == aff.getTutore()) {
				this.listeAffectation.remove(a);
				break;
			}
		}
		for(Tutore t : this.tutores) {
			if(t.equals(aff.tutore)) {
				t.getAffectation().remove(aff.tuteur);
				break;
			}
		}
		for(Tuteur t : this.tuteurs) {
			if(t.equals(aff.tuteur)) {
				t.getAffectation().remove(aff.tutore);
				break;
			}
		}
	}
	
	/**
	 * Method to add a new Tutor
	 * @param t
	 */
	public void addTuteur(Tuteur t ) {
		this.tuteurs.add(t);
	}
	/**
	 * Method to add a new Tutee
	 * @param t
	 */
	public void addTutore(Tutore t) {
		this.tutores.add(t);
	}
	
	
	
	public void clearAllAffectation() {
		listeAffectation.clear();
		for(Tuteur t : this.tuteurs) {
			t.getAffectation().clear();
		}
		for(Tutore t : this.tutores) {
			t.getAffectation().clear();
		}
		this.nonAffectation.clear();
	}
	
	public Eleve getFromNomPrenom(String nom, String prenom) {
		
		for(Tuteur t : tuteurs) {
			if(t.getNom().equals(nom) && t.getPrenom().equals(prenom)) {
				return t;
			}
		}
		for(Tutore t : tutores) {
			if(t.getNom().equals(nom) && t.getPrenom().equals(prenom)) {
				return t;
			}
		}
		return null;
	}
	
	public void clearNonAffectation() {
		this.nonAffectation.clear();
	}
	
	public ArrayList<Tutore> getNonAffectationTutore(){
		ArrayList<Tutore> res = new ArrayList<>();
		for(Eleve e : this.nonAffectation) {
			if(e.getAnnee() == 1) {
				res.add((Tutore) e);
			}
		}
		return res;
	}
	
	public ArrayList<Tuteur> getNonAffectationTutor(){
		ArrayList<Tuteur> res = new ArrayList<>();
		for(Eleve e : this.nonAffectation) {
			if(e.getAnnee() != 1) {
				res.add((Tuteur) e);
			}
		}
		return res;
	}
	
	public boolean addAffectation(Affectation aff) {
		for(Affectation a : listeAffectation) {
			if(a.getTuteur() == aff.getTuteur() && a.getTutore() == aff.getTutore()) {
				return false;
			}
		}
		return listeAffectation.add(aff);
	}
}

