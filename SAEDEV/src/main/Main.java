package main;

import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Scanner;


import eleves.*;
import fr.ulille.but.sae2_02.graphes.GrapheNonOrienteValue;
import graphes.*;
/**
 * Main class to launch the program
 * @author arthur.blanchet.etu
 *
 */
public class Main {
	/**
	 * Scanner for inputs
	 */
	static Scanner sc = new Scanner(System.in);
	/**
	 * Affectation instance 
	 */
	static AffectationManager aff = new AffectationManager();
	/**
	 * List of teachers
	 */
	static ListeEnseignant le= new ListeEnseignant();
	/**
	 * Login
	 */
	static String log;
	/**
	 * Subject to use 
	 */
	static Matiere mat;
	static int a = 0;
	static {SaveLoad.loadAffectation(aff);}
	public static void main(String[] args) {
		start();
		
	}
	
	
	
	/**
	 * Method to start the program
	 */
	private static void start() {
		printPath("/donnees/texte/main");
		while((a = sc.nextInt())>3 || a<0) {}
		if(a == 3) {
			sc.close();
			SaveLoad.saveAffectation(aff);
			System.exit(0);
		} 
		if(a == 1) {
			eleve();
		} else if (a == 2) {
			enseignant(true);
		}
		
	}
	/**
	 * Menu for Students
	 */
	private static void eleve() {
		printPath("/donnees/texte/eleve");
		while((a = sc.nextInt())>3 || a<0) {}
		if(a==1) {
			creation();
			eleve();
		} else if (a == 2){
			consultation();
			eleve();
		} else {
			start();
		}
	}
	/**
	 * Method for creating and adding a new Student
	 */
	public static void creation() {

		System.out.println("Veuillez donner votre nom");
		String nom = sc.next();
		System.out.println("Veuillez donner votre prenom");
		String prenom = sc.next();
		System.out.println("Veuillez donner votre annee");
		int annee = 0;
		while((annee = sc.nextInt())<0|| annee > 3) {}
		HashMap<Matiere,Double> mat = new HashMap<>();
		if(annee==1) {
			System.out.println("Pour combien de matieres? Max :" +Matiere.values().length);
			int cpt = 0;
			while((cpt = sc.nextInt()) <0 || cpt>Matiere.values().length) {}
			for(int i = 0 ;i < cpt ; i++) {
				System.out.println("Quel est le nom de la matiere? (dev,ihm,graphes ou general");
				for(Matiere m : Matiere.values()) {
					System.out.println(m);
				}
				Matiere matiere = Matiere.valueOf(sc.next().toUpperCase());
				System.out.println("Quelle est votre note?");
				double note = sc.nextDouble();
				mat.put(matiere, note);
			}
			aff.getTutores().add(new Tutore(nom,prenom,mat));
		} else {
			System.out.println("Pour quelle matiere? (dev,ihm,graphes ou general)");
			Matiere matiere = Matiere.valueOf(sc.next().toUpperCase() );
			System.out.println("Quelle est votre note?");
			double note = sc.nextDouble();
			mat.put(matiere, note);
		}
		System.out.println("Votre inscription s'est bien passee. Appuyez sur entree pour revenir en arrie�re");
		sc.next();
		eleve();

	}
	
	/**
	 * Method to show all affectations of the student who was input
	 */
	public static void consultation() {
		System.out.println("Veuillez donner votre nom");
		String nom = sc.next();
		System.out.println("Veuillez donner votre prenom");
		String prenom = sc.next();
		System.out.println("Veuillez donner votre annee d'etude");
		int annee = 0;
		while((annee = sc.nextInt())<0|| annee > 3) {}
		boolean flag = false;
		int cpt = 0;
		if(annee == 1) {
			while(flag == false && cpt < aff.getTutores().size()) {
				Tutore t = aff.getTutores().get(cpt);
				if(t.getNom().equals(nom)&&t.getPrenom().equals(prenom)) {
					System.out.println(t.getAffectation());
					flag = true;
				}
				cpt++;
			}
		} else {
			while(flag == false && cpt < aff.getTuteurs().size()) {
				Tuteur t = aff.getTuteurs().get(cpt);
				if(t.getNom().equals(nom)&&t.getPrenom().equals(prenom)) {
					System.out.println(t.getAffectation());
					flag = true;
				}
				cpt++;
			}
		}
		System.out.println("Appuyez sur entrer pour revenir en arriere");
		sc.next();
		eleve();
	}
	
	/**
	 * Teacher menu
	 */
	private static void enseignant(boolean test) {
		if(test) {
			System.out.println("Veuillez vous connecter (Utilisez general general comme identifiants c'est mieux pour utiliser l'outil");
			try {
				login();
			} catch (WrongInputLengthException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (WrongLoginException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (WrongPwdException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			aff.filtreMatiere(mat); //C'est fait pour afficher que les eleves qui ont cette matie�re
		}
		printPath("/donnees/texte/enseignant");
		while((a = sc.nextInt())>5 || a<0) {}	
		if(a==1) {
			printAllEleves();
			enseignant(false);
		} else if (a == 2){
			printAllAffectation();
			enseignant(false);
		} else if (a == 3) {
			printAllNonAffectation();
			enseignant(false);
		} else if (a == 4){
			configuration();
		} else {
			start();
		}
	}
	
	/**
	 * Login method for a Teacher
	 * @throws WrongInputLengthException
	 * @throws WrongLoginException
	 * @throws WrongPwdException
	 */
	public static void login() throws WrongInputLengthException, WrongLoginException, WrongPwdException {
		Scanner sc = new Scanner(new InputStreamReader(System.in));
		String test1 = "";
		String test2 = "";
		System.out.println("(Le nom d'utilisateur et le mdp sont les memes, entrez dev,ihm,graphes ou general 2 fois)");
		System.out.println("Saisissez votre nom d'utilisateur");
		test1= sc.nextLine();
		if(test1 == null || !le.enseignant.containsKey(test1)) {
			System.out.println("Vous n'existez pas");
			throw new WrongLoginException();
		} 
		System.out.println("Saisissez votre mot de passe");
		test2 = sc.nextLine();
		if(test2 == null || !le.enseignant.get(test1).equals(test2)) {
			System.out.println("Mauvais mot de passe");
			throw new WrongPwdException();
		}
		
		log = test1;
		mat = le.matiere.get(test1);
		
	}
	/**
	 * Configuration menu for teachers
	 */
	private static void configuration() {
		printPath("/donnees/texte/configuration");
		while((a = sc.nextInt())>6 || a<0) {}
		if(a == 1) {
			supprimerAffectation();
		} else if (a == 2) {
			rajouterDansAffectation();
		} else if (a == 3) {
			filtrageNote();
		} else if (a == 4) {
			try {
				fixeAffectation();
			} catch (StudentNoExists e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (AffectationAlreadyExists e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (a == 5){
			affectation();
		} else {
			enseignant(false);
		}
	}
	
	/**
	 * Method to not assign a student
	 */
	private static void supprimerAffectation() {
		printAllEleves();
		System.out.println("Veuillez donner un nom");
		String nom = sc.next();
		System.out.println("Veuillez donner un prenom");
		String prenom = sc.next();
		if(aff.getFromNomPrenom(nom, prenom)!=null) {
			aff.supprAffectation(aff.getFromNomPrenom(nom, prenom));
			System.out.println("L'eleve a bien ete enleve de l'affectation. Veuillez entrer une lettre pour revenir en arriere");
		} 
		sc.next();
		configuration();
	}
	
	/**
	 * Method to put back a student in the assignments
	 */
	private static void rajouterDansAffectation() {
		printAllNonAffectation();
		System.out.println("Veuillez donner un nom");
		String nom = sc.next();
		System.out.println("Veuillez donner un prenom");
		String prenom = sc.next();
		Eleve e = aff.getFromNomPrenom(nom, prenom);
		if(e.getAnnee()==1) {
			aff.addTutore((Tutore) e);
		} else {
			aff.addTuteur((Tuteur) e);
		}
		System.out.println("L'eleve a bien ete ajoute. Veuillez entrer une lettre pour revenir en arriere");
		sc.next();
		configuration();
	}

	/**
	 * Method to filter from inputs
	 */
	private static void filtrageNote() {
		System.out.println("Veuillez entrer le seuil de filtrage pour les tuteurs");
		aff.filtreTuteur(sc.nextDouble(), mat);
		System.out.println("Veuillez entrer le seuil de filtrage pour les tutores");
		aff.filtreTutore(sc.nextDouble(), mat);
		System.out.println("Les filtres ont bien ete mis en place. Veuillez entrer une lettre pour revenir en arriere");
		sc.next();
		configuration();
	}
	
	/**
	 * Method to force an assignments between 2 students inputted
	 * @throws StudentNoExists
	 * @throws AffectationAlreadyExists
	 */
	private static void fixeAffectation() throws StudentNoExists, AffectationAlreadyExists {
		printAllEleves();
		System.out.println("Veuillez donner le nom du tuteur");
		String nom1 = sc.next();
		System.out.println("Veuillez donner le prenom du tuteur");
		String prenom1 = sc.next();
		System.out.println("Veuillez donner le nom du tutore");
		String nom2 = sc.next();
		System.out.println("Veuillez donner le pre�nom du tutore");
		String prenom2 = sc.next();
		Tuteur t = aff.getTuteurs().get(aff.getTuteurs().indexOf((Tuteur) aff.getFromNomPrenom(nom1, prenom1)));
		Tutore tu = aff.getTutores().get(aff.getTutores().indexOf((Tutore) aff.getFromNomPrenom(nom2, prenom2)));
		if(!aff.getTuteurs().contains(t) || !aff.getTutores().contains(tu)) {
			throw new StudentNoExists();
		} else if (aff.getTuteurs().get(aff.getTuteurs().indexOf(t)).getAffectation().contains(tu)) {
			throw new AffectationAlreadyExists();
		}
		aff.getTuteurs().get(aff.getTuteurs().indexOf(t)).addAffectation(tu);
		aff.getTutores().get(aff.getTutores().indexOf(tu)).addAffectation(t);
		System.out.println("L'affectation a bien ete mise. Veuillez entrer une lettre pour revenir en arriere");
		sc.next();
		configuration();
	}
	
	/**
	 * Affectation menu
	 */
	private static void affectation() {
		printPath("/donnees/texte/affectation");
		while((a = sc.nextInt())>3 || a<0) {}
		if(a == 3) {
			configuration();
		}
		if(a == 1) {
			aff.clearAllAffectation();
			
		} 
		GrapheNonOrienteValue<Eleve> graphe = new GrapheNonOrienteValue<>();
		Graphe.creationGraphe(aff, graphe, mat);
		aff.creationAffectation(mat);
		System.out.println("Les affectations ont ete creees. Veuillez entrer une lettre pour revenir en arriere");
		sc.next();
		affectation();
	}



	
	
	
	/**
	 * Method to print all excluded from affectation
	 */
	private static void printAllNonAffectation() {
		System.out.println("Liste des exclus de l'affectation");
		for(Eleve e : aff.getNonAffectation()) {
			System.out.println(e);
		}
	}
	/**
	 * Method to print all affectations
	 */
	private static void printAllAffectation() {
		System.out.println("Liste des affectations:");
		for(Tuteur t : aff.getTuteursFiltres()) {
			System.out.println(t.getNom() + " " + t.getAffectation());
		}
		System.out.println("Tutores non affectes");
		for(Tutore t : aff.getTutoresFiltres()) {
			if(t.getAffectation().isEmpty()) {
				System.out.println(t);
			}
		}
	}
	/**
	 * Method to print all students
	 */
	private static void printAllEleves() {
		System.out.println("Tuteurs");
		for(Tuteur t : aff.getTuteursFiltres()) {
			System.out.println(t);
		}
		System.out.println("Tutores");
		for(Tutore t : aff.getTutoresFiltres()) {
			System.out.println(t);
		}		
	}
	
	/**
	 * Method to print a file
	 * @param file
	 */
	public static void printPath(String file) {
		Path path = Paths.get(System.getProperty("user.dir")+file);
		try {
			for(String ligne : Files.readAllLines(path)) {
				System.out.println(ligne);
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		}
		
	}
	
	
	
	
	
	
}
