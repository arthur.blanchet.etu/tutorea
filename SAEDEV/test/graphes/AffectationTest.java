package graphes;
import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import fr.ulille.but.sae2_02.graphes.Arete;
import fr.ulille.but.sae2_02.graphes.CalculAffectation;
import fr.ulille.but.sae2_02.graphes.GrapheNonOrienteValue;
import eleves.*;

/**
 * @author loic.lecointe.etu
 * @author arthur.blanchet.etu
 * @author ehis.okpebho.etu
 *
 *
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class AffectationTest {
	public GrapheNonOrienteValue<Eleve> graphe;
	public ArrayList<Tuteur> tuteur = new ArrayList<>();
	public ArrayList<Tutore> tutore = new ArrayList<>();
	public AffectationManager affectation;
	public CalculAffectation<Eleve> calculAffectation;
	public List<Arete<Eleve>> aretes;
	ArrayList<Eleve> newTutore = new ArrayList<>();
	ArrayList<Eleve> newTuteurs = new ArrayList<>();

	/**
	 * 
	 */
	@BeforeEach
	void Initialization() {
		
		graphe = new GrapheNonOrienteValue<>();
		HashMap<Matiere,Double> general = new HashMap<>();
		general.put(Matiere.GENERAL,12.9);
		tuteur.add(new Tuteur("Roux","Daniel",3,general));
		general.replace(Matiere.GENERAL,11.9);

		tuteur.add(new Tuteur("Hebert","Franck",3,general));
		general.replace(Matiere.GENERAL,15.8);

		tuteur.add(new Tuteur("Leleu","Sabine",2,general));
		general.replace(Matiere.GENERAL,11.9);

		tuteur.add(new Tuteur("Jacques","Daisy",2,general));
		general.replace(Matiere.GENERAL,10.4);

		tuteur.add(new Tuteur("Lefebvre","Georges",2,general));

		general.replace(Matiere.GENERAL,0.2);
		tutore.add(new Tutore("Bouchet","Lucas",general));
		general.replace(Matiere.GENERAL,6.9);
		tutore.add(new Tutore("Barre","Madeleine",general));
		general.replace(Matiere.GENERAL,12.7);
		tutore.add(new Tutore("Besnard","Sabine",general));
		general.replace(Matiere.GENERAL,17.3);
		tutore.add(new Tutore("Bigot","Hugues",general));
		affectation = new AffectationManager(tuteur,tutore);
		affectation.harmoniser(Matiere.GENERAL);
		for(Tuteur t : affectation.tuteurs) {
			newTuteurs.add(t);
		}
		for(Tutore t : affectation.tutores) {
			newTutore.add(t);
		}
		
		Graphe.creationGraphe(affectation, graphe, Matiere.GENERAL);
		calculAffectation = new CalculAffectation<Eleve>(graphe,newTutore,newTuteurs);
		aretes = calculAffectation.getAffectation();	
	}
	
	/**
	 * 
	 */
	@AfterEach
	public void afterATest() {
		tuteur = new ArrayList<>();
		tutore = new ArrayList<>();
		graphe = new GrapheNonOrienteValue<>();
		aretes = new ArrayList<>();
		newTutore = new ArrayList<>();
		newTuteurs = new ArrayList<>();
		affectation = new AffectationManager();
	}
	
	/**
	 * 
	 */
	@Order(1)
	@Test 
	void testValeurAffectation() {
	
		//Les valeurs devaient être arrondis en entier car il y avait des 24.9000000002
		assertEquals(17,Math.round(affectation.calculAff(tuteur.get(0), tutore.get(0),Matiere.GENERAL)));
		assertEquals(21,Math.round(affectation.calculAff(tuteur.get(1), tutore.get(1),Matiere.GENERAL)));
		assertEquals(26,Math.round(affectation.calculAff(tuteur.get(2), tutore.get(1),Matiere.GENERAL)));
		assertEquals(32,Math.round(affectation.calculAff(tuteur.get(4), tutore.get(3),Matiere.GENERAL)));
	
		assertTrue(aretes.contains(graphe.getArete(tutore.get(0), tuteur.get(1))));
		assertTrue(aretes.contains(graphe.getArete(tutore.get(1), tuteur.get(2))));
		assertTrue(aretes.contains(graphe.getArete(tutore.get(2), tuteur.get(3))));
		assertTrue(aretes.contains(graphe.getArete(tutore.get(3), tuteur.get(0))));
	}
	
	/**
	 * 
	 */
	@Order(2)
	@Test
	void testFixe() {
		
		graphe = new GrapheNonOrienteValue<>();
		graphe.ajouterSommet(tuteur.get(0));
		graphe.ajouterSommet(tutore.get(3));
		graphe.ajouterArete(tutore.get(3),tuteur.get(0),-100);
		Graphe.creationGraphe(affectation, graphe, Matiere.GENERAL);

		calculAffectation = new CalculAffectation<Eleve>(graphe,newTutore,newTuteurs);
		aretes = calculAffectation.getAffectation();

		
		assertTrue(aretes.contains(graphe.getArete(tutore.get(0), tuteur.get(1))));
		assertTrue(aretes.contains(graphe.getArete(tutore.get(1), tuteur.get(2))));
		assertTrue(aretes.contains(graphe.getArete(tutore.get(2), tuteur.get(3))));
		assertTrue(aretes.contains(graphe.getArete(tutore.get(3), tuteur.get(0))));
	}
	
	/**
	 * 
	 */
	@Order(3)
	@Test
	void testNonAffectation() {
		//On enlève Bouchet, Jacques, Besnard et Hebert
		graphe = new GrapheNonOrienteValue<>();
		tuteur.remove(3);
		tuteur.remove(1);
		tutore.remove(2);
		tutore.remove(0);
		newTuteurs = new ArrayList<>();
		newTutore = new ArrayList<>();
		Graphe.creationGraphe(affectation, graphe, Matiere.GENERAL);
		for(Tuteur t : tuteur) {
			newTuteurs.add(t);
		}
		for(Tutore t : tutore) {
			newTutore.add(t);
		}

		calculAffectation = new CalculAffectation<Eleve>(graphe,newTutore,newTuteurs);
		aretes = calculAffectation.getAffectation();

		
	
		assertTrue(aretes.contains(graphe.getArete(tutore.get(1), tuteur.get(0))));
		assertTrue(aretes.contains(graphe.getArete(tutore.get(0), tuteur.get(1))));

	}
}
